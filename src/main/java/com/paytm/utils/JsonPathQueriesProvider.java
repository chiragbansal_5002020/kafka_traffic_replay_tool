package com.paytm.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class JsonPathQueriesProvider {
	
	private static List<String> queries = new ArrayList<String>();
	static String queryFilePath = ConfigReader.getValue("jsonPathQueriesFilePath");
	static {
		try {
			
			File queryFile = new File(queryFilePath);
			if(!queryFile.exists()) {
				System.out.println("JsonPath Queries file not found at following path "+queryFilePath+
						"\n"+"Proceeding with comparing complete json response!!");
			}
			BufferedReader reader = new BufferedReader(new FileReader(queryFile));
			String line = "";
			while((line=reader.readLine())!=null) {
				if(!line.trim().isEmpty())
					queries.add(line.trim());
			}
			System.out.println("Number of Jsonpath queries found : "+ queries.size());
			reader.close();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			
		}
	}
	
	public static List<String> getJsonPathQueries() {
		return queries;
	}
	
	public static void main(String[] args) {
		List<String> q = getJsonPathQueries();
		if(q.size()>0) {
			System.out.println(q);
		}else {
			System.out.println("No query could be read");
		}
	}
}
