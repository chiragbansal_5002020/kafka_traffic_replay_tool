package com.paytm.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
	static Properties props = new Properties();
	static String configFilePath = "./src/main/resources/config.properties";
	static {
		File configFile = new File(configFilePath);
		if(!configFile.exists()) {
			System.out.println("Config.properties file couldn't be found at path "+configFilePath
					+"\n"+"It may affect the execution and so exiting the program!!");
			System.exit(0);
		}else {
			try {
				props.load(new FileInputStream(configFile));
			} catch (IOException e) {
				System.out.println("Some error occured while reading from config file. Exiting the program!!");
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
	
	public static String getValue(String key) {
		return props.getProperty(key);
	}
	
	public static boolean shouldIgnoreNewKeys() {
		String value = ConfigReader.getValue("shouldIgnoreNewKeys").toLowerCase().trim();
		boolean result = false;
		if(value.equals("true") || value.equals("yes"))
			result = true;
		else if(value.equals("false") || value.equals("no"))
			result = false;
		return result;
	}
	public static void main(String[] args) {
		System.out.println(ConfigReader.getValue("isNewEntriesConsideredInComparison"));
	}
}
