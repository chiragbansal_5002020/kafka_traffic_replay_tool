package com.paytm.trafficreplaykafka;

import java.io.IOException;
import java.util.Collections;
import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import com.paytm.utils.Comparator;
import com.paytm.utils.ConfigReader;



public class KafkaConsumerGoReplay {
	    
	 private final String TOPIC = ConfigReader.getValue("topic");
		    private final String BOOTSTRAP_SERVERS =
		            ConfigReader.getValue("host");
	private Comparator comparator;
	
	
	public KafkaConsumerGoReplay(Comparator c) {
		this.comparator = c;
	}
	    
	    private Consumer<Long, String> createConsumer() {
	        final Properties props = new Properties();
	        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
	                                    BOOTSTRAP_SERVERS);
	        if(ConfigReader.getValue("shouldReadLatest").toLowerCase().equals("true") || 
	        		ConfigReader.getValue("shouldReadLatest").toLowerCase().trim().equals("yes"))
	        	 props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
	        else
	        	props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
	        
//	        props.put(ConsumerConfig.GROUP_ID_CONFIG,
//	                                    "KafkaConsumerRequestReplayTool");

	        props.put(ConsumerConfig.GROUP_ID_CONFIG,
	        		UUID.randomUUID().toString());
	        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
	                LongDeserializer.class.getName());
	        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
	                StringDeserializer.class.getName());
	       
	     
	        final Consumer<Long, String> consumer =
	                                    new KafkaConsumer<>(props);
	 
	        consumer.subscribe(Collections.singletonList(TOPIC));
	        
	        return consumer;
	    }
	    
	    
	    
	    public void runConsumer() throws InterruptedException {
	        final Consumer<Long, String> consumer = createConsumer();
	        //final int giveUp = 10000;   int noRecordsCount = 0;
	        while (true) {
//	        	if(Runner.appScanner.hasNext()) {
//	        		if(Runner.appScanner.next().toLowerCase().contains("exit")) {
//	        			System.exit(0);
//	        		}
//	        	}
	        	System.out.println("Waiting for kafka messages");
	            final ConsumerRecords<Long, String> consumerRecords =
	                    consumer.poll(1000);
//	            if (consumerRecords.count()==0) {
//	                noRecordsCount++;
//	                if (noRecordsCount > giveUp) break;
//	                else continue;
//	            }
	            
	            
	            consumerRecords.forEach(record -> {
	            	System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	                
	                System.out.println("Timestamp : "+record.timestamp());
	                if(record.value().contains("GET") && record.value().contains("HTTP")) {
	                	String url = record.value().substring(
		                		record.value().indexOf("GET")+4,record.value().indexOf("HTTP")-1).trim();
	                	System.out.println("GET URL -- ["+url+"]");
	                	try {
							this.comparator.compare(url);
						} catch (IOException e) {
							e.printStackTrace();
						}
	                }else {
	                	System.out.println("Not a get request");
	                }
	                
	                System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	                
	            });
	            consumer.commitAsync();
	        }
	        
	        //consumer.close();
	       // System.out.println("DONE");
	    }
}
