package com.paytm.trafficreplaykafka;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import com.paytm.trafficreplaykafka.KafkaConsumerGoReplay;
import com.paytm.utils.Comparator;

public class Runner {
	public static Scanner appScanner ;
	private static Comparator comparator;
	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println("Enter exit to close the program!");
		appScanner = new Scanner(System.in);
		
		Runtime.getRuntime().addShutdownHook(new Thread() 
	    { 
	      public void run() 
	      { 
	        System.out.println("Shutdown Hook is running !"); 
	        comparator.writeStatusToStatusFile();
	      } 
	    });
		
		String dirName = "./output";
		
		if(args.length > 0) {
			dirName = args[0];
		}
		
		new File(dirName).mkdirs();
		
		comparator = new Comparator(dirName);
		KafkaConsumerGoReplay consumer = new KafkaConsumerGoReplay(comparator);
		
		Thread t = new Thread() {
				@Override
				public void run() {
					try {
						consumer.runConsumer();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
		};
		
		t.start();
		while(true) {
			System.out.println("Waiting for user input");
			if(appScanner.nextLine().toLowerCase().trim().equals("exit")) {
				t.stop();
				System.exit(0);
			}
		}
	  } 
}
